package com.massivecraft.massiveessentialsacknowledge.Engine;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.event.EventMassiveCoreAknowledge;
import com.massivecraft.massivecore.util.MUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class EngineAcknowledgeEvent extends Engine
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static EngineAcknowledgeEvent i = new EngineAcknowledgeEvent();
    public static EngineAcknowledgeEvent get() { return i; }

    // -------------------------------------------- //
    // LISTENER
    // -------------------------------------------- //

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void listener(EventMassiveCoreAknowledge event)
    {
        Object sender = event.getSender();
        Object sendee = event.getSendee();

        // Essentials takes care of checking for null. Only players can be ignored.
        if(MUtil.isntPlayer(sender)) return;
        if(MUtil.isntPlayer(sendee)) return;

        User senderUser = Essentials.getPlugin(Essentials.class).getUser((Player)sendee);
        User sendeeUser = Essentials.getPlugin(Essentials.class).getUser((Player)sender);

        if( ! sendeeUser.isIgnoredPlayer(senderUser)) return;

        event.setCancelled(true);
    }
}