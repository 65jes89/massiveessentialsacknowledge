package com.massivecraft.massiveessentialsacknowledge;

import com.massivecraft.massivecore.MassivePlugin;
import com.massivecraft.massiveessentialsacknowledge.Engine.EngineAcknowledgeEvent;

public class EssentialsAcknowledge extends MassivePlugin
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static EssentialsAcknowledge i;
    public static EssentialsAcknowledge get() { return i; }

    // -------------------------------------------- //
    // Construct
    // -------------------------------------------- //

    public EssentialsAcknowledge() { EssentialsAcknowledge.i = this; }

    // -------------------------------------------- //
    // Override
    // -------------------------------------------- //

    @Override
    public void onEnableInner()
    {
        this.activate(
                EngineAcknowledgeEvent.class
        );
    }
}